terraform {
  required_providers {
    heroku = {
      source  = "heroku/heroku"
      version = "~> 5.0"
    }
  }
  backend "s3" {
    bucket = "heroku-telegram-bots"
    key    = "nit-o-dia-tlegram-bot/tf/"
    region = "eu-west-3"
  }
}

variable "heroku-app-name" {
  type        = string
  description = "Heroku App Name"
}

variable "telegram-api-token" {
  type        = string
  description = "Telegram API Key to connect to telegram"
}

variable "sentry-dsn" {
  type        = string
  description = "Sentry DSN"
}

resource "heroku_app" "heroku-app" {
  name   = var.heroku-app-name
  region = "eu"
  stack  = "heroku-22"

  buildpacks = [
    "https://github.com/moneymeets/python-poetry-buildpack.git",
    "heroku/python"
  ]

  config_vars = {
    UTC_OFFSET  = 2
    LANG        = "ca_ES"
    WEBHOOK_URL = "https://${var.heroku-app-name}.herokuapp.com"
    ENV         = "production"
  }

  sensitive_config_vars = {
    TELEGRAM_API_TOKEN = var.telegram-api-token
    SENTRY_DSN         = var.sentry-dsn
  }
}
