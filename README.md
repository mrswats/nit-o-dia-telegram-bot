# Nit o dia?

Un bot molt senzill que et dona informacio sobre el sol pel dia d'avui i et diu si es de dia o es de nit.

## Execució en local

Per tal de poder executar en local el bot, es necessari l'ús d'un fixter `.env` dins el mateix directori.
Aquest fitxer ha de contenir el token que et proveeix el Father Bot:

```
TELEGRAM_API_TOKEN="<el token va aqui>"
```

Aquest bot tambe necessita una instancia de Redis corrent a localhost per funcionar de forma local.

Després, instal·lar dependències fent

```console
poetry install
```

Finalment, executar el bot amb:

```console
poetry run python ./bot.py
```
