import locale
import logging
import os
from dataclasses import dataclass, field
from datetime import datetime, timedelta, timezone

import dotenv
import requests
import sentry_sdk
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, Updater

dotenv.load_dotenv()

locale.setlocale(locale.LC_ALL, "ca_ES")

logging.basicConfig(
    level=logging.INFO if os.getenv("ENV") else logging.DEBUG,
    style="{",
    format="{name} -- [{levelname}] {asctime}: {message}",
)

LOCAL = "Local"
ENVIRONMENT = os.environ.get("ENV", LOCAL)
TELEGRAM_API_TOKEN = os.getenv("TELEGRAM_API_TOKEN")
WEBHOOK_URL = os.getenv("WEBHOOK_URL")
PORT = int(os.getenv("PORT", "8443"))
UTC_OFFSET = os.getenv("UTC_OFFSET", "1")
SUNSET_API = "https://api.sunrise-sunset.org/json"
QUERY_PARAMS = {
    "lat": "41.466667",
    "lon": "2.083333 ",
    "formatted": 0,
}


@dataclass
class SunsetAPI:

    _key: str = field(default="", init=False)
    _data: dict = field(default_factory=dict, init=False)

    @property
    def data(self) -> dict:
        if not self._data:
            self._data = self._get_sunset_info_from_api()

        return self._data

    @property
    def key(self) -> str:
        if not self._key:
            self._key = datetime.now().strftime("%Y-%m-%d")

        return self._key

    def _get_sunset_info_from_api(self) -> dict:
        logging.debug("Requesting API data")
        return requests.get(SUNSET_API, QUERY_PARAMS, verify=False).json().get("results")


@dataclass
class Bot:

    sunset_api: SunsetAPI
    updater: Updater
    _full_info: dict = field(default_factory=dict, init=False)
    utc_offset: int = int(UTC_OFFSET)

    def __post_init__(self) -> None:
        self.register_handlers()

    @property
    def full_info(self) -> dict[str, str]:
        if not self._full_info:
            self._full_info = self.sunset_api.data

        return self._full_info

    @property
    def sunset(self) -> datetime:
        date = datetime.fromisoformat(self.full_info["sunset"])
        return date + timedelta(hours=self.utc_offset)

    @property
    def sunrise(self) -> datetime:
        date = datetime.fromisoformat(self.full_info["sunrise"])
        return date + timedelta(hours=self.utc_offset)

    @property
    def solar_noon(self) -> datetime:
        date = datetime.fromisoformat(self.full_info["solar_noon"])
        return date + timedelta(hours=self.utc_offset)

    @property
    def day_length(self) -> str:
        return self.full_info["day_length"]

    @property
    def today(self) -> str:
        return datetime.now().strftime("%A %d de %B del %Y")

    def register_handlers(self) -> None:
        for command in [
            CommandHandler("info", self.info_cmd),
            CommandHandler("nitodia", self.nit_o_dia_cmd),
        ]:
            self.updater.dispatcher.add_handler(command)

    @staticmethod
    def format_date(dt: datetime) -> str:
        return dt.strftime("%H:%M")

    def nit_o_dia_cmd(self, update: Update, context: CallbackContext) -> None:
        now = datetime.now(tz=timezone.utc)

        es_de_dia = self.sunrise < now < self.sunset

        missatge = "Es de dia! 🌞" if es_de_dia else "Es de nit! 🌝"

        update.message.reply_text(missatge)

    def info_cmd(self, update: Update, context: CallbackContext) -> None:
        message = (
            f" 🌞 Informació del Sol pel {self.today} 🌝\n"
            f"Avui el sol surt a les {self.format_date(self.sunrise)}\n"
            f"Avui el sol es pon a les {self.format_date(self.sunset)}\n"
            f"El migdia solar es a les {self.format_date(self.solar_noon)}\n"
            f"El dia solar avui dura {self.day_length}s\n"
        )

        update.message.reply_text(message)

    def _start_webhook(self) -> None:
        logging.info("Starting in webhook mode")
        self.updater.start_webhook(
            listen="0.0.0.0",
            port=PORT,
            url_path=TELEGRAM_API_TOKEN,
            webhook_url=f"{WEBHOOK_URL}/{TELEGRAM_API_TOKEN}",
        )

    def _start_polling(self) -> None:
        logging.info("Starting in local polling mdoe")
        self.updater.start_polling()

    def start_bot(self) -> None:
        if ENVIRONMENT == LOCAL:
            self._start_polling()
        else:
            self._start_webhook()

        self.updater.idle()


def main():
    sentry_sdk.init(
        dsn=os.getenv("SENTRY_DSN"),
        traces_sample_rate=1.0,
        environment=ENVIRONMENT,
    )

    bot = Bot(
        sunset_api=SunsetAPI(),
        updater=Updater(TELEGRAM_API_TOKEN),
    )

    bot.start_bot()


if __name__ == "__main__":
    main()
